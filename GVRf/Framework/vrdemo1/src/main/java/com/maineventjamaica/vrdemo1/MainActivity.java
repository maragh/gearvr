package com.maineventjamaica.vrdemo1;

import android.os.Bundle;

import org.gearvrf.GVRActivity;

public class MainActivity extends GVRActivity {


    Sample main = new Sample();

    @Override
    protected void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        setMain(main,"gvr.xml");
    }
}

package com.maineventjamaica.vrdemo1;

import android.graphics.Color;

import org.gearvrf.GVRContext;
import org.gearvrf.GVRMain;
import org.gearvrf.GVRScene;
import org.gearvrf.GVRSceneObject;
import org.gearvrf.GVRTexture;

/**
 * Created by jermaine on 08/08/2016.
 */

public class Sample extends GVRMain {

    private GVRContext mGVRContext;

    @Override
    public void onInit(GVRContext gvrContext) throws Throwable {
        // save context for possible use in onStep(), even though that is empty*
        // in this sample*
        mGVRContext = gvrContext;
        GVRScene scene = gvrContext.getMainScene();
        scene.getMainCameraRig().getLeftCamera()
                .setBackgroundColor(Color.WHITE);
        scene.getMainCameraRig().getRightCamera()
                .setBackgroundColor(Color.WHITE);

        GVRTexture texture = gvrContext.loadTexture("GearVR.jpg");
        // create a a scene object (this constructor creates a rectangular scene*
        // object that uses the standard 'unlit' shader)*
        GVRSceneObject sceneObject = new GVRSceneObject(gvrContext,4.0f,2.0f,texture);

        // set the scene object position
        sceneObject.getTransform().setPosition(0.0f,0.0f,-3.0f);

        // add the scene object to the scene graph
        scene.addSceneObject(sceneObject);
    }

    @Override
    public void onStep() {

    }
}

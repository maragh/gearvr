package com.maineventjamaica.myvr;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;

import org.gearvrf.GVRActivity;

public class MainActivity extends GVRActivity {

    SampleCubeMain main = new SampleCubeMain();

    @Override
    protected void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        setMain(main, "gvr.xml");
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_UP) {
            main.captureScreen(0, "screenshot_center");
            main.captureScreen(1, "screenshot_left");
            main.captureScreen(2, "screenshot_right");
            main.captureScreen3D("screenshot3d");
        }
        return super.onTouchEvent(event);
    }
}
